<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
    protected $table = 'pet_types';
    public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    public function pet()
    {
        return $this->hasMany(Pet::class);
    }

    public function breed()
    {
        return $this->hasMany(PetBreed::class);
    }

}
