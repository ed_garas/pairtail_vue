<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $table = 'pets';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'gender',
        'age',
        'video',
        'price',
        'status',
        'description',
        'family',
        'prizes',
        'is_active',
        'user_id',
        'breed_id',
        'type_id',
//        'save_only_image'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(PetType::class);
    }

    public function breed()
    {
        return $this->belongsTo(PetBreed::class);
    }

    public function images()
    {
        return $this->hasMany(PetImage::class);
    }

    static public function getGenders(){
        return [
            [
                'id' => 1,
                'name' => 'male'
            ],
            [
                'id' => 2,
                'name' => 'female',
            ]
        ];
    }

    static public function getStatus(){
        return [
            [
                'id' => 1,
                'name' => 'for_sale'
            ],
            [
                'id' => 2,
                'name' => 'for_date',
            ]
        ];
    }

}
