<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetImage extends Model
{
    protected $table = 'pet_images';
    public $timestamps = true;

    protected $fillable = [
        'path',
        'is_avatar',
        'pet_id',
    ];

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}
