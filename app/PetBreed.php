<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetBreed extends Model
{
    protected $table = 'pet_breeds';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'type_id',
    ];

    public function type()
    {
        return $this->belongsTo(PetType::class);
    }

    public function pet()
    {
        return $this->hasMany(Pet::class);
    }

}
