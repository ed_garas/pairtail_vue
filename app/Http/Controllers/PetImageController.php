<?php

namespace App\Http\Controllers;

use App\Pet;
use App\PetImage;
use Illuminate\Http\Request;

class PetImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function remove($id){
        $image = PetImage::findOrFail($id);
        $image->delete();

        $file = public_path('storage/' . $image->path);
        if(file_exists($file)){
            unlink($file);
        }

        $pet = Pet::findOrFail($image->pet_id);

        if($pet->avatar == $image->path){
            $new_avatar = PetImage::where('pet_id', '=', $pet->id)->first();
            $pet->avatar = $new_avatar->path;
            $pet->save();
        }


        return response()
            ->json([
                'removed' => true,
            ]);
    }
}
