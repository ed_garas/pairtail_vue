<?php

namespace App\Http\Controllers;

use App\Pet;
use App\PetBreed;
use App\PetImage;
use App\PetType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class PetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()
            ->json([
                'pets' => Pet::where('is_active', '=', '1')->with('type', 'breed')->get()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = PetType::get(['id', 'name']);
        return response()
            ->json([
                'types' => $types,
            ]);
    }

    public function getBreeds($id)
    {
        $breeds = PetBreed::where('type_id','=',$id)->get();
        return response()
            ->json([
                'breeds' => $breeds
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type_id' => 'required|integer|min:1',
            'breed_id' => 'required|integer|min:1',
        ]);

        $pet = new Pet;
        $pet->fill($request->all());
        $pet->user_id = Auth::user()->id;
        $pet->is_active = 0;
        $pet->save();

        return response()
            ->json([
                'saved' => true,
                'id' => $pet->id,
                'message' => 'pet saved',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()
            ->json([
                'pet' => Pet::findOrFail($id)
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pet = Pet::findOrFail($id);
        $types = PetType::get(['id', 'name']);
        $genders = Pet::getGenders();
        $statuses = Pet::getStatus();
        return response()
            ->json([
                'pet' => $pet,
                'genders' => $genders,
                'types' => $types,
                'images' => $pet->images,
                'statuses' => $statuses
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:40',
            'gender' => 'required|string|min:3',
            'type_id' => 'required|integer|min:1',
            'breed_id' => 'required|integer|min:1',
            'age' => 'integer',
            'video' => '',
            'description' => 'sometimes|nullable|min:6|max:255',
            'family' => 'sometimes|nullable|min:6|max:255',
            'prizes' => 'sometimes|nullable|min:6|max:255',
        ]);

        $pet = Pet::findOrFail($id);
        $pet->fill($request->all());
        $pet->is_active = 1;
        $pet->save();

        return response()
            ->json([
                'saved' => true
            ]);
    }

    public function getPetImages($id){
        $pet = Pet::findOrFail($id);
        return response()
            ->json([
                'images' => $pet->images
            ]);
    }

    public function avatar($id){
        $image = PetImage::findOrFail($id);
        $pet = Pet::findOrFail($image->pet_id);

        $pet->avatar = $image->path;
        $pet->save();
        return response()
            ->json([
                'saved' => true,
                'avatar' => $pet->avatar,
            ]);
    }

    public function upload(Request $request, $id)
    {
        $files = $request->file('path');
        $status = false;
        $filePath = '';
        if(count($files)){
            foreach ($files as $file){
                $item = new PetImage;
                $ext = $file->guessClientExtension();
                $uniqueName = substr(md5(microtime()), 0, 20);
                $path = 'pets/user_'. Auth()->id() . '/pet_' . $id .'/';
                $imageName = $uniqueName.'.'.$ext;
                $file->storeAs('public/'.$path, $imageName);
                $item->path = $path . $imageName;

                $filePath = public_path('storage/' . $path . $imageName);
                if(file_exists($filePath)){
                    Image::make($filePath)->fit(600)->save($filePath);
                }
                $item->pet_id = $id;
                $item->save();
            }

            $pet = Pet::findOrFail($id);
            if(empty($pet->avatar)){
                $image = PetImage::where('pet_id', '=', $pet->id)->first();
                $pet->avatar = $image->path;
                $pet->save();
            }
            $status = true;
        }

        return response()->json([
            'saved'=>$status,
            'pet_id'=>$id,
            'image_num' => count($files),
            'path' => $filePath,
        ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pet = Pet::findOrFail($id);
        $pet->delete();

        return response()
            ->json([
                'deleted' => true
            ]);
    }
}
