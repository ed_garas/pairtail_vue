# Single Page Application with Laravel 5.4 and Vue.js 2

### Frameworks and Libraries:

- Laravel 5.4
- Vue.js 2.2
- Vue Router
- Axios

### Installation
`git clone https://github.com/codekerala/laravel-and-vue.js-spa-Recipe-Box.git`

`laravel-and-vue.js-spa-Recipe-Box`

`composer install`

`npm install`
