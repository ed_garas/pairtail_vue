<?php

use App\PetType;
use App\PetBreed;
use Illuminate\Database\Seeder;

class PetDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PetType::create(['name' => 'dog']);
        PetType::create(['name' => 'cat']);
        PetType::create(['name' => 'horse']);
        PetType::create(['name' => 'bird']);


        PetBreed::create(['type_id' => 1, 'name' => 'Vokieciu aviganis']);
        PetBreed::create(['type_id' => 1, 'name' => 'Biglis']);
        PetBreed::create(['type_id' => 1, 'name' => 'Mopsas']);

        PetBreed::create(['type_id' => 2, 'name' => 'Siamo']);
        PetBreed::create(['type_id' => 2, 'name' => 'Britu nulepausis']);
        PetBreed::create(['type_id' => 2, 'name' => 'Rusu melynasis']);

        PetBreed::create(['type_id' => 3, 'name' => 'Zirgas']);
        PetBreed::create(['type_id' => 3, 'name' => 'Mustangas']);

        PetBreed::create(['type_id' => 4, 'name' => 'Papuga']);
        PetBreed::create(['type_id' => 4, 'name' => 'Kanarele']);
        PetBreed::create(['type_id' => 4, 'name' => 'Varna']);
    }
}
