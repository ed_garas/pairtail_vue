<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path')->nullable();
            $table->integer('pet_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('pet_images', function (Blueprint $table) {
            $table->foreign('pet_id')->references('id')->on('pets')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_images', function (Blueprint $table) {
            $table->dropForeign(['pet_id']);
        });
        Schema::dropIfExists('pet_images');
    }
}
