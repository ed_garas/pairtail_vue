<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('price')->nullable();
            $table->string('status')->nullable();
            $table->string('video')->nullable();
            $table->text('description')->nullable();
            $table->text('family')->nullable();
            $table->text('prizes')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->string('avatar')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('breed_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('pets', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('pet_types');
            $table->foreign('breed_id')->references('id')->on('pet_breeds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pets', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['breed_id']);
        });
        Schema::dropIfExists('pets');
    }
}
