<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pairtail | Where pets find love</title>
        <script src="https://use.fontawesome.com/acb77ef93c.js"></script>
        <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div id="root"></div>
    </body>
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</html>
