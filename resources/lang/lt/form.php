<?php

return [

    // default form fields
    'name' => 'Vardas',
    'email' => 'El. Paštas',
    'password' => 'Slaptažodis',
    'login' => 'Prisijungti',
    'register' => 'Registruotis',
    'save' => 'Išsaugoti',

    'logout_msg' => 'Sėkmingai atsijungėte',
    'login_msg' => 'Sėkmingai prisijungėte',
    'register_msg' => 'Sveikiname, Sėkmingai užsiregistravote!',

    // login form
    'login_page' => [
        'msg' => 'Sveiki sugrįžę!',
    ],

    // login form
    'register_page' => [
        'msg' => 'Sukurkite paskyra',
        'name' => 'Vardas ir pavardė',
        'confirm_password' => 'Patvirtinkite slaptažodį',
    ],

    // user
    'user' => [
        'name' => 'Vardas',
        'email' => 'El. paštas',
        'phone' => 'Tel. numeris',
        'country' => 'Šalis',
        'city' => 'Miestas',
        'street' => 'Gatvė',
        'company' => 'Įmonė',

        'updated_msg' => 'Sėkmingai atnaujinote profilio informacija!',
    ],

];
