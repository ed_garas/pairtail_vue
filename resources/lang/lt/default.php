<?php

return [

    // navigation
    'pet_list' => 'Gyvūnai',
    'new_pet' => 'Naujas gyvūnas',
    'profile' => 'Profilis',
    'logout' => 'Atsijungti',
    'login' => 'Prisijungti',
    'register' => 'Registruotis',

    'update_profile_link' => 'Atnaujinti profilio informacija',

    'dropzone_msg' => 'Įkelti nuotraukas',
    'dropzone_error_file_limit' => 'Failas per didelis :size. Maksimalus dydis: :max_size.',

];
