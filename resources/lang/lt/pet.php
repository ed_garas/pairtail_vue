<?php

return [

    'gender' => 'Lytis',
    'male' => 'Patinas',
    'female' => 'Patelė',

    'name' => 'Vardas',
    'age' => 'Amžius',
    'video' => 'Vaizdo įrašas',
    'description' => 'Aprašymas',
    'family' => 'Šeima',
    'prizes' => 'Apdovanojimai',
    'active' => 'Aktyvus',
    'owner' => 'Savininkas',
    'type' => 'Tipas',
    'breed' => 'Veislė',

    'activate' => 'Aktyvuoti',
    'delete' => 'Ištrinti',
    'edit' => 'Redaguoti',

    'my_pets' => 'Mano gyvūnai',
    'create_title' => 'Sukurti nauja gyvūną',
    'no_pets_msg' => 'Gyvūnų sąrašas tuščias...',

    'show_page' => 'Rodyti',

    'type_title' => [
        'dog' => 'Šuo',
        'cat' => 'Katė',
        'horse' => 'Arklys',
        'bird' => 'Paukštis',
    ]

];
