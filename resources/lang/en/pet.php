<?php

return [

    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',

    'name' => 'Name',
    'age' => 'Age',
    'video' => 'Video',
    'description' => 'Description',
    'family' => 'Family',
    'prizes' => 'Prizes',
    'active' => 'Active',
    'owner' => 'Owner',
    'type' => 'Type',
    'breed' => 'Breed',

    'activate' => 'Activate',
    'delete' => 'Delete',
    'edit' => 'Edit',

    'my_pets' => 'My pets',
    'create_title' => 'Create new pet',
    'no_pets_msg' => 'Pet list is empty...',

    'show_page' => 'Show',

    'type_title' => [
        'dog' => 'Dog',
        'cat' => 'Cat',
        'horse' => 'Horse',
        'bird' => 'Bird',
    ]

];
