<?php

return [

    // default form fields
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'login' => 'Login',
    'register' => 'Register',
    'save' => 'Save',

    'logout_msg' => 'You have successfully logged out.',
    'login_msg' => 'You have successfully logged in!',
    'register_msg' => 'Congratulations! You have now successfully registered.',

    // login form
    'login_page' => [
        'msg' => 'Welcome back!',
    ],

    // login form
    'register_page' => [
        'msg' => 'Create an account',
        'name' => 'Full name',
        'confirm_password' => 'Confirm password',
    ],

    // user
    'user' => [
        'name' => 'Full name',
        'email' => 'Email',
        'phone' => 'Phone',
        'country' => 'Country',
        'city' => 'City',
        'street' => 'Street',
        'company' => 'Company',

        'updated_msg' => 'You have successfully updated profile.',
    ],

];
