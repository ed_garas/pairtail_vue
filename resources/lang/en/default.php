<?php

return [

    // navigation
    'pet_list' => 'Pet List',
    'new_pet' => 'New Pet',
    'profile' => 'Profile',
    'logout' => 'Logout',
    'login' => 'Login',
    'register' => 'Register',

    'update_profile_link' => 'Update profile',

    'dropzone_msg' => 'Upload photos',
    'dropzone_error_file_limit' => 'File is too big :size. Max filesize: :max_size',
];
