import Vue from 'vue'

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
import Dropzone from 'vue2-dropzone'

import App from './App.vue'
import router from './router'
import VueI18n from 'vue-i18n';
import messages from './vue-i18n-locales.generated.js';
Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: 'en',
    messages,
});

Vue.component('pet_create', require('./views/Pet/Create.vue'));

const app = new Vue({
    el: '#root',
    template: `<app></app>`,
    i18n,
    components: { App },
    router,
});