import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '../views/Auth/Login.vue';
import Register from '../views/Auth/Register.vue';
import Home from '../views/Home.vue';
import NotFound from '../views/NotFound.vue';


import Profile from '../views/Profile/Index.vue';
import ProfileEdit from '../views/Profile/Edit.vue';

import PetList from '../views/Pet/Index.vue';
import PetCreate from '../views/Pet/Create.vue';
import PetEdit from '../views/Pet/Edit.vue';
import PetShow from '../views/Pet/Show.vue';

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		{ path: '/', component: PetList },
		{ path: '/register', component: Register },
		{ path: '/login', component: Login },
		{ path: '/not-found', component: NotFound },
		{ path: '*', component: NotFound },

		{ path: '/profile', component: Profile },
		{ path: '/profile/:id/edit', component: ProfileEdit },

		{ path: '/pets', component: PetList },
		{ path: '/pet/create', component: PetCreate },
		{ path: '/pet/:id/edit', component: PetEdit },
		{ path: '/pet/:id', component: PetShow },
	]
});

export default router
