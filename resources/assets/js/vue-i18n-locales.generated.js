export default {
    "en": {
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "pet": {
            "gender": "Gender",
            "male": "Male",
            "female": "Female",
            "name": "Name",
            "age": "Age",
            "video": "Video",
            "description": "Description",
            "family": "Family",
            "prizes": "Prizes",
            "active": "Active",
            "owner": "Owner",
            "type": "Type",
            "breed": "Breed",
            "activate": "Activate",
            "delete": "Delete",
            "edit": "Edit",
            "my_pets": "My pets",
            "create_title": "Create new pet",
            "no_pets_msg": "Pet list is empty...",
            "show_page": "Show",
            "type_title": {
                "dog": "Dog",
                "cat": "Cat",
                "horse": "Horse",
                "bird": "Bird"
            }
        },
        "default": {
            "pet_list": "Pet List",
            "new_pet": "New Pet",
            "profile": "Profile",
            "logout": "Logout",
            "login": "Login",
            "register": "Register",
            "update_profile_link": "Update profile",
            "dropzone_msg": "Upload photos",
            "dropzone_error_file_limit": "File is too big {size}. Max filesize: {max_size}"
        },
        "form": {
            "name": "Name",
            "email": "Email",
            "password": "Password",
            "login": "Login",
            "register": "Register",
            "save": "Save",
            "logout_msg": "You have successfully logged out.",
            "login_msg": "You have successfully logged in!",
            "register_msg": "Congratulations! You have now successfully registered.",
            "login_page": {
                "msg": "Welcome back!"
            },
            "register_page": {
                "msg": "Create an account",
                "name": "Full name",
                "confirm_password": "Confirm password"
            },
            "user": {
                "name": "Full name",
                "email": "Email",
                "phone": "Phone",
                "country": "Country",
                "city": "City",
                "street": "Street",
                "company": "Company",
                "updated_msg": "You have successfully updated profile."
            }
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        }
    },
    "lt": {
        "pet": {
            "gender": "Lytis",
            "male": "Patinas",
            "female": "Patelė",
            "name": "Vardas",
            "age": "Amžius",
            "video": "Vaizdo įrašas",
            "description": "Aprašymas",
            "family": "Šeima",
            "prizes": "Apdovanojimai",
            "active": "Aktyvus",
            "owner": "Savininkas",
            "type": "Tipas",
            "breed": "Veislė",
            "activate": "Aktyvuoti",
            "delete": "Ištrinti",
            "edit": "Redaguoti",
            "my_pets": "Mano gyvūnai",
            "create_title": "Sukurti nauja gyvūną",
            "no_pets_msg": "Gyvūnų sąrašas tuščias...",
            "show_page": "Rodyti",
            "type_title": {
                "dog": "Šuo",
                "cat": "Katė",
                "horse": "Arklys",
                "bird": "Paukštis"
            }
        },
        "default": {
            "pet_list": "Gyvūnai",
            "new_pet": "Naujas gyvūnas",
            "profile": "Profilis",
            "logout": "Atsijungti",
            "login": "Prisijungti",
            "register": "Registruotis",
            "update_profile_link": "Atnaujinti profilio informacija",
            "dropzone_msg": "Įkelti nuotraukas",
            "dropzone_error_file_limit": "Failas per didelis {size}. Maksimalus dydis: {max_size}."
        },
        "form": {
            "name": "Vardas",
            "email": "El. Paštas",
            "password": "Slaptažodis",
            "login": "Prisijungti",
            "register": "Registruotis",
            "save": "Išsaugoti",
            "logout_msg": "Sėkmingai atsijungėte",
            "login_msg": "Sėkmingai prisijungėte",
            "register_msg": "Sveikiname, Sėkmingai užsiregistravote!",
            "login_page": {
                "msg": "Sveiki sugrįžę!"
            },
            "register_page": {
                "msg": "Sukurkite paskyra",
                "name": "Vardas ir pavardė",
                "confirm_password": "Patvirtinkite slaptažodį"
            },
            "user": {
                "name": "Vardas",
                "email": "El. paštas",
                "phone": "Tel. numeris",
                "country": "Šalis",
                "city": "Miestas",
                "street": "Gatvė",
                "company": "Įmonė",
                "updated_msg": "Sėkmingai atnaujinote profilio informacija!"
            }
        }
    }
}
