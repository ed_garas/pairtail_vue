<?php

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('register', 'AuthController@register');

Route::resource('profile', 'ProfileController');
Route::post('setLang', 'ProfileController@setLang');
Route::get('getLang', 'ProfileController@getLang');

Route::resource('pet', 'PetController');
Route::get('pet/getBreeds/{id}', 'PetController@getBreeds');
Route::post('upload/{id}', 'PetController@upload');

Route::get('image/remove/{id}', 'PetImageController@remove');
Route::get('avatar/{id}', 'PetController@avatar');